/*
 * Copyright (c) 2000, 2003, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package com.digiarea.gct.jsdoc;


/**
 * A class whose instances are filters over Modifier bits. Filtering is done by
 * returning boolean values. Classes, methods and fields can be filtered, or
 * filtering can be done directly on modifier bits.
 * 
 * @see com.sun.tools.javac.code.Flags;
 * @author Robert Field
 */

public class ModifierFilter {

	private ModifierFilterType filterType;

	public ModifierFilter(ModifierFilterType filterType) {
		this.filterType = filterType;
	}

	/**
	 * Filter on modifier bits.
	 * 
	 * @param modifierBits
	 *            Bits as specified in the Modifier class
	 * 
	 * @return Whether the modifierBits pass this filter.
	 */
	public boolean checkModifier(int modifierBits) {
		switch (filterType) {
		case PRIVATE:
			return true;
		case PROTECTED:
			return !Flags.isPrivate(modifierBits);
		case PUBLIC:
			return !Flags.isPrivate(modifierBits) && !Flags.isProtected(modifierBits);
		}
		return true;
	}

} // end ModifierFilter
