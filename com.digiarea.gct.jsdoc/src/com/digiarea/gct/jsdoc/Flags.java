package com.digiarea.gct.jsdoc;

public final class Flags {

	public static final int AccDefault = 0;
	public static final int AccPublic = 0x0001;
	public static final int AccPrivate = 0x0002;
	public static final int AccProtected = 0x0004;
	public static final int AccStatic = 0x0008;
	public static final int AccFinal = 0x0010;
	public static final int AccAbstract = 0x0400;
	public static final int AccSuper = 0x0020;
	public static final int AccDeprecated = 0x100000;
	public static final int AccConstant = 0x0020;
	public static final int AccVarargs = 0x0080;
	public static final int AccEnum = 0x4000;
	public static final int AccInterface = 0x0200;
	public static final int AccDict = 0x8000;
	public static final int AccStruct = 0x10000;
	public static final int AccNamespace = 0x0040;
	public static final int AccObject = 0x0080;
	public static final int AccProvided = 0x1000;

	private Flags() {
	}

	public static boolean isInterface(int flags) {
		return (flags & AccInterface) != 0;
	}

	public static boolean isAbstract(int flags) {
		return (flags & AccAbstract) != 0;
	}

	public static boolean isDeprecated(int flags) {
		return (flags & AccDeprecated) != 0;
	}

	public static boolean isConstant(int flags) {
		return (flags & AccConstant) != 0;
	}

	public static boolean isPackageDefault(int flags) {
		return (flags & (AccPublic | AccProtected | AccPrivate)) == 0;
	}

	public static boolean isPrivate(int flags) {
		return (flags & AccPrivate) != 0;
	}

	public static boolean isProtected(int flags) {
		return (flags & AccProtected) != 0;
	}

	public static boolean isPublic(int flags) {
		return (flags & AccPublic) != 0;
	}

	public static boolean isStatic(int flags) {
		return (flags & AccStatic) != 0;
	}

	public static boolean isFinal(int flags) {
		return (flags & AccFinal) != 0;
	}

	public static boolean isSuper(int flags) {
		return (flags & AccSuper) != 0;
	}

	public static boolean isVarargs(int flags) {
		return (flags & AccVarargs) != 0;
	}

	public static boolean isStruct(int flags) {
		return (flags & AccDict) != 0;
	}

	public static boolean isDict(int flags) {
		return (flags & AccStruct) != 0;
	}

	public static boolean isNamespace(int flags) {
		return (flags & AccNamespace) != 0;
	}

	public static boolean isObject(int flags) {
		return (flags & AccObject) != 0;
	}

	public static boolean isProvided(int flags) {
		return (flags & AccProvided) != 0;
	}

	public static boolean isEnum(int flags) {
		return (flags & AccEnum) != 0;
	}

	public static String toString(int flags) {
		StringBuffer sb = new StringBuffer();

		if (isPublic(flags))
			sb.append("public "); //$NON-NLS-1$
		else if (isProtected(flags))
			sb.append("protected "); //$NON-NLS-1$
		else if (isPrivate(flags))
			sb.append("private "); //$NON-NLS-1$
		else
			sb.append("public "); //$NON-NLS-1$

		/* Canonical order */
		if (isAbstract(flags))
			sb.append("abstract ");//$NON-NLS-1$
		if (isStatic(flags))
			sb.append("static ");//$NON-NLS-1$
		if (isFinal(flags))
			sb.append("final ");//$NON-NLS-1$
		if (isInterface(flags))
			sb.append("interface ");//$NON-NLS-1$

		int len = sb.length();
		if (len == 0)
			return ""; //$NON-NLS-1$
		sb.setLength(len - 1);
		return sb.toString();
	}

}
